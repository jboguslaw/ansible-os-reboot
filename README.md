Role Name
=========

OS Reboot

Requirements
------------

N/A

Role Variables
--------------

	fuse - (enable|disable) to to disable for reboot

Dependencies
------------

N/A

Example Playbook
----------------

```yaml
- name: OS Reboot
  hosts: all
  #strategy: debug
  gather_facts: yes
  become: yes
  roles:
    - { role: ansible-os-reboot, tags: [ 'all' ] }
```

License
-------

BSD

Author Information
------------------

Jakub K. Boguslaw <jboguslaw@gmail.com> please inform me if you find any error/mistake
